config2 = [85, -85, -88, 88, -4, 4, -88, 88, -49, 49, 114, -114, 60, -60, -96, 96];
[T0RH2, T0LH2, T0RF2, T0LF2] = visualize(config2);

syms t1 t2 t3 t4 t5 t6 t7 t8 t9 t10 t11 t12 t13 t14 t15 t16
config4 = [t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15, t16];
[T0RHs, T0LHs, T0RFs, T0LFs] = symTmat(config4);
T0RHs = simplify(T0RHs);
T0LHs = simplify(T0LHs);
T0RFs = simplify(T0RFs);
T0LFs = simplify(T0LFs);
T15 = atan2(-T0RF2(1,2),-T0RF2(1,1));
T0RFs2 = subs(T0RFs,t15,T15);
T0RFs2 = subs(T0RFs2,t7,deg2rad(-88));
