function [T0RH, T0LH, T0RF, T0LF] = visualize(v)
    theta1 = v(1);
    theta2 = v(2);
    theta3 = v(3);
    theta4 = v(4);
    theta5 = v(5);
    theta6 = v(6);
    theta7 = v(7);
    theta8 = v(8);
    theta9 = v(9);
    theta10 = v(10);
    theta11 = v(11);
    theta12 = v(12);
    theta13 = v(13);
    theta14 = v(14);
    theta15 = v(15);
    theta16 = v(16);
    a1 = {'Color','r'};
    a2 = {'Color','g'};
    a3 = {'Color','b'};
    % base frame axes
    o0 = [0;0;0;1];
    x0 = [4; 0; 0; 1];
    y0 = [0; 4; 0; 1];
    z0 = [0; 0; 4; 1];
    hold on
    plotline(o0,x0,a1)
    plotline(o0,y0,a2)
    plotline(o0,z0,a3)
    hold off
    %% RH
    DHRH = [0, 90, 57, theta1-90;
        12, 90, 0, theta3+90;
        45, 0, 0, theta5+90;
        0, 90, 75, 0];
    T01 = dh(DHRH(1,:));
    T13 = dh(DHRH(2,:));
    T35 = dh(DHRH(3,:));
    T5RH = dh(DHRH(4,:));

    % frame 1 axes
    o1 = T01*o0;
    x1 = T01*x0;
    y1 = T01*y0;
    z1 = T01*z0;
    hold on
    plotline(o1,x1,a1)
    plotline(o1,y1,a2)
    plotline(o1,z1,a3)
    plotline(o0,o1,a3)
    hold on

    % frame 3 axes
    T03 = T01*T13;
    o3 = T03*o0;
    x3 = T03*x0;
    y3 = T03*y0;
    z3 = T03*z0;
    hold on
    plotline(o3,x3,a1)
    plotline(o3,y3,a2)
    plotline(o3,z3,a3)
    plotline(o1,o3,a3)
    hold on

    % frame 5 axes
    T05 = T03*T35;
    o5 = T05*o0;
    x5 = T05*x0;
    y5 = T05*y0;
    z5 = T05*z0;
    hold on
    plotline(o5,x5,a1)
    plotline(o5,y5,a2)
    plotline(o5,z5,a3)
    plotline(o3,o5,a3)
    hold on

    % frame RH axes
    T0RH = T05*T5RH;
    oRH = T0RH*o0;
    xRH = T0RH*x0;
    yRH = T0RH*y0;
    zRH = T0RH*z0;
    hold on
    plotline(oRH,xRH,a1)
    plotline(oRH,yRH,a2)
    plotline(oRH,zRH,a3)
    plotline(o5,oRH,a3)
    hold on

    %% LH
    DHLH = [0, -90, 57, theta2+90;
        12, -90, 0, theta4-90;
        45, 0, 0, theta6+90;
        0, 90, 75, 180];
    T02 = dh(DHLH(1,:));
    T24 = dh(DHLH(2,:));
    T46 = dh(DHLH(3,:));
    T6LH = dh(DHLH(4,:));

    % frame 2 axes
    o2 = T02*o0;
    x2 = T02*x0;
    y2 = T02*y0;
    z2 = T02*z0;
    hold on
    plotline(o2,x2,a1)
    plotline(o2,y2,a2)
    plotline(o2,z2,a3)
    plotline(o0,o2,a3)
    hold on

    % frame 4 axes
    T04 = T02*T24;
    o4 = T04*o0;
    x4 = T04*x0;
    y4 = T04*y0;
    z4 = T04*z0;
    hold on
    plotline(o4,x4,a1)
    plotline(o4,y4,a2)
    plotline(o4,z4,a3)
    plotline(o2,o4,a3)
    hold on

    % frame 6 axes
    T06 = T04*T46;
    o6 = T06*o0;
    x6 = T06*x0;
    y6 = T06*y0;
    z6 = T06*z0;
    hold on
    plotline(o6,x6,a1)
    plotline(o6,y6,a2)
    plotline(o6,z6,a3)
    plotline(o4,o6,a3)
    hold on

    % frame LH axes
    T0LH = T06*T6LH;
    oLH = T0LH*o0;
    xLH = T0LH*x0;
    yLH = T0LH*y0;
    zLH = T0LH*z0;
    hold on
    plotline(oLH,xLH,a1)
    plotline(oLH,yLH,a2)
    plotline(oLH,zLH,a3)
    plotline(o6,oLH,a3)
    hold on

    %% RF
    DHRF = [15, 90, 24, -90;
        72, -90, 0, theta7;
        6, -90, 0, theta9;
        45, 0, 0, theta11;
        42, 180, 0, theta13;
        0, 90, 0, theta15;
        31, 180, 0, 0];
    T0a = dh(DHRF(1,:));
    Ta7 = dh(DHRF(2,:));
    T79 = dh(DHRF(3,:));
    T911 = dh(DHRF(4,:));
    T1113 = dh(DHRF(5,:));
    T1315 = dh(DHRF(6,:));
    T15RF = dh(DHRF(7,:));

    % frame a axes
    oa = T0a*o0;
    xa = T0a*x0;
    ya = T0a*y0;
    za = T0a*z0;
    hold on
    plotline(oa,xa,a1)
    plotline(oa,ya,a2)
    plotline(oa,za,a3)
    plotline(o0,oa,a3)
    hold on

    % frame 7 axes
    T07 = T0a*Ta7;
    o7 = T07*o0;
    x7 = T07*x0;
    y7 = T07*y0;
    z7 = T07*z0;
    hold on
    plotline(o7,x7,a1)
    plotline(o7,y7,a2)
    plotline(o7,z7,a3)
    plotline(oa,o7,a3)
    hold on

    % frame 9 axes
    T09 = T07*T79;
    o9 = T09*o0;
    x9 = T09*x0;
    y9 = T09*y0;
    z9 = T09*z0;
    hold on
    plotline(o9,x9,a1)
    plotline(o9,y9,a2)
    plotline(o9,z9,a3)
    plotline(o7,o9,a3)
    hold on

    % frame 11 axes
    T011 = T09*T911;
    o11 = T011*o0;
    x11 = T011*x0;
    y11 = T011*y0;
    z11 = T011*z0;
    hold on
    plotline(o11,x11,a1)
    plotline(o11,y11,a2)
    plotline(o11,z11,a3)
    plotline(o9,o11,a3)
    hold on

    % frame 13 axes
    T013 = T011*T1113;
    o13 = T013*o0;
    x13 = T013*x0;
    y13 = T013*y0;
    z13 = T013*z0;
    hold on
    plotline(o13,x13,a1)
    plotline(o13,y13,a2)
    plotline(o13,z13,a3)
    plotline(o11,o13,a3)
    hold on

    % frame 15 axes
    T015 = T013*T1315;
    o15 = T015*o0;
    x15 = T015*x0;
    y15 = T015*y0;
    z15 = T015*z0;
    hold on
    plotline(o15,x15,a1)
    plotline(o15,y15,a2)
    plotline(o15,z15,a3)
    plotline(o13,o15,a3)
    hold on

    % frame RF axes
    T0RF = T015*T15RF;
    oRF = T0RF*o0;
    xRF = T0RF*x0;
    yRF = T0RF*y0;
    zRF = T0RF*z0;
    hold on
    plotline(oRF,xRF,a1)
    plotline(oRF,yRF,a2)
    plotline(oRF,zRF,a3)
    plotline(o15,oRF,a3)
    hold on

    %% LF
    DHLF = [15, -90, 24, 90;
        72, 90, 0, theta8;
        6, 90, 0, theta10;
        45, 0, 0, theta12;
        42, 180, 0, theta14+180;
        0, 90, 0, theta16+180;
        31, 180, 0, 0];
    T0a2 = dh(DHLF(1,:));
    Ta28 = dh(DHLF(2,:));
    T810 = dh(DHLF(3,:));
    T1012 = dh(DHLF(4,:));
    T1214 = dh(DHLF(5,:));
    T1416 = dh(DHLF(6,:));
    T16LF = dh(DHLF(7,:));

    % frame a2 axes
    oa2 = T0a2*o0;
    xa2 = T0a2*x0;
    ya2 = T0a2*y0;
    za2 = T0a2*z0;
    hold on
    plotline(oa2,xa2,a1)
    plotline(oa2,ya2,a2)
    plotline(oa2,za2,a3)
    plotline(o0,oa2,a3)
    hold on

    % frame 8 axes
    T08 = T0a2*Ta28;
    o8 = T08*o0;
    x8 = T08*x0;
    y8 = T08*y0;
    z8 = T08*z0;
    hold on
    plotline(o8,x8,a1)
    plotline(o8,y8,a2)
    plotline(o8,z8,a3)
    plotline(oa2,o8,a3)
    hold on

    % frame 10 axes
    T010 = T08*T810;
    o10 = T010*o0;
    x10 = T010*x0;
    y10 = T010*y0;
    z10 = T010*z0;
    hold on
    plotline(o10,x10,a1)
    plotline(o10,y10,a2)
    plotline(o10,z10,a3)
    plotline(o8,o10,a3)
    hold on

    % frame 12 axes
    T012 = T010*T1012;
    o12 = T012*o0;
    x12 = T012*x0;
    y12 = T012*y0;
    z12 = T012*z0;
    hold on
    plotline(o12,x12,a1)
    plotline(o12,y12,a2)
    plotline(o12,z12,a3)
    plotline(o10,o12,a3)
    hold on

    % frame 14 axes
    T014 = T012*T1214;
    o14 = T014*o0;
    x14 = T014*x0;
    y14 = T014*y0;
    z14 = T014*z0;
    hold on
    plotline(o14,x14,a1)
    plotline(o14,y14,a2)
    plotline(o14,z14,a3)
    plotline(o12,o14,a3)
    hold on

    % frame 16 axes
    T016 = T014*T1416;
    o16 = T016*o0;
    x16 = T016*x0;
    y16 = T016*y0;
    z16 = T016*z0;
    hold on
    plotline(o16,x16,a1)
    plotline(o16,y16,a2)
    plotline(o16,z16,a3)
    plotline(o14,o16,a3)
    hold on

    % frame LF axes
    T0LF = T016*T16LF;
    oLF = T0LF*o0;
    xLF = T0LF*x0;
    yLF = T0LF*y0;
    zLF = T0LF*z0;
    hold on
    plotline(oLF,xLF,a1)
    plotline(oLF,yLF,a2)
    plotline(oLF,zLF,a3)
    plotline(o16,oLF,a3)
    hold on
end