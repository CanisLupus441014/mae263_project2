function T = dh2(v)
    a = v(1);
    alph = v(2);
    d = v(3);
    theta = v(4);
    T = [cos(theta), -sin(theta), 0, a;
        sin(theta)*cos(alph), cos(theta)*cos(alph), -sin(alph), -sin(alph)*d;
        sin(theta)*sin(alph), cos(theta)*sin(alph), cos(alph), cos(alph)*d;
        0, 0, 0, 1];
end