function plotline(o,v,a)
    x0 = o(1);
    y0 = o(2);
    z0 = o(3);
    x = v(1);
    y = v(2);
    z = v(3);
    line([x0,x], [y0,y], [z0,z],a(1),a(2))
end