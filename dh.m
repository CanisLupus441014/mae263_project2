function T = dh(v)
    a = v(1);
    alph = v(2);
    d = v(3);
    theta = v(4);
    T = [cosd(theta), -sind(theta), 0, a;
        sind(theta)*cosd(alph), cosd(theta)*cosd(alph), -sind(alph), -sind(alph)*d;
        sind(theta)*sind(alph), cosd(theta)*sind(alph), cosd(alph), cosd(alph)*d;
        0, 0, 0, 1];
end