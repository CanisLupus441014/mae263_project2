close all;
config1 = [0, 0, -73, 73, 0, 0, 0, 0, -26, 26, 29, -29, 13, -13, 0, 0];
[T0RH1, T0LH1, T0RF1, T0LF1] = visualize(config1);

figure;
config2 = [85, -85, -88, 88, -4, 4, -88, 88, -49, 49, 114, -114, 60, -60, -96, 96];
[T0RH2, T0LH2, T0RF2, T0LF2] = visualize(config2);

% Symbolic Transformation Matrix

syms t1 t2 t3 t4 t5 t6 t7 t8 t9 t10 t11 t12 t13 t14 t15 t16
config4 = [t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15, t16];
[T0RHs, T0LHs, T0RFs, T0LFs] = symTmat(config4);
T0RHs = simplify(T0RHs);
T0LHs = simplify(T0LHs);
T0RFs = simplify(T0RFs);
T0LFs = simplify(T0LFs);

ground = T0RF2(3,4);
ball_diam = 40;
tee_h = 31;
ball_center_height = ball_diam/2 + tee_h + ground;
T_pos = [T0RH2(1,4),0,ball_center_height];
hold on
t = linspace(0,2*pi);
x = 20*cos(t)+T_pos(1);
y = 20*sin(t)+T_pos(2);
z = T_pos(3)*ones(1,100);
plot3(x,y,z)
y2 = zeros(1,100);
z2 = 20*sin(t)+T_pos(3);
plot3(x,y2,z2);
x3 = T_pos(1)*ones(1,100);
y3 = 20*sin(t)+T_pos(2);
z3 = 20*cos(t)+T_pos(3);
plot3(x3,y3,z3);
%appendage
